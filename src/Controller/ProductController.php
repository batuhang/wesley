<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\AfrekenenFormType;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="product_index", methods={"GET"})
     */
    public function index(ProductRepository $productRepository): Response
    {
        return $this->render('product/index.html.twig', [
            'products' => $productRepository->findAll(),
            'cart' => $this->session->get('cart')
        ]);
    }

    /**
     * @Route("/checkout/", name="product_checkout", methods={"GET","POST"})
     */
    public function checkout(\Swift_Mailer $mailer, Request $request){
        $cart = $this->session->get('cart', []);
        $message = (new Swift_Message())
            ->setSubject('Here should be a subject')
            ->setTo('recipient@example.com')
            ->setFrom(['support@mailtrap.io'])
            ->setBody(
            $this->renderView(
                'product/mail.html.twig',[
                    'cart' => $cart,
                    'name' => $request->request->get('afrekenen_form')['name'],
                    'email' => $request->request->get('afrekenen_form')['email'],
                    ]),
            'text/html'
        );

        $mailer->send($message);

        return $this->render('product/checkout.html.twig', [
            'cart' => $cart,
            'name' => $request->request->get('afrekenen_form')['name'],
            'email' => $request->request->get('afrekenen_form')['email'],
        ]);
    }

    /**
     * @Route("/new", name="product_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="product_show", methods={"GET"})
     */
    public function show(Product $product): Response
    {
        return $this->render('product/show.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="product_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Product $product): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
        }

        return $this->redirectToRoute('product_index');
    }
    /**
     * @Route("/{id}/cart", name="product_cart", methods={"GET","POST"})
     */
    public function AddToCart(Product $product, Request $request)
    {
        $cart = $this->session->get('cart', []);
        $form = $this->createForm(AfrekenenFormType::class);

        if(isset($cart[$product->getId()])){
            $cart[$product->getId()]['aantal']++;
        }
        else{
            $cart[$product->getId()] = array(
                'aantal' => 1,
                'name' =>$product->getName(),
                'price' => $product->getPrice(),
                'id' =>$product->getId(),
            );
        }
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted() && $form->isValid()) {
                // perform some action...

                return $this->redirectToRoute('product_checkout', [
                    'form' => $form->getData()
                ]);
            }
        }
        $this->session->set('cart', $cart);
        return $this->render('product/cart.html.twig',[
            'product' => $cart[$product->getId()]['name'],
            'aantal' => $cart[$product->getId()]['aantal'],
            'cart' => $cart,
            'form' => $form->createView()
        ]);
    }
}
